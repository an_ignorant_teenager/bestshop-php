 :star:  :star:  :star:  :star:  :star: :star:  :star:  :star:  :star:  :star:  :star:

### 新产品发布推荐

现已推出 [ **萤火商城多端版V2.0** ]，新系统更轻量级、高性能、前后端分离，且支持微信小程序 + H5+ 公众号 + APP，依旧前后端源码完全开源！

仓库地址：[https://gitee.com/xany/yoshop2.0](https://gitee.com/xany/yoshop2.0)

如果您没有多端需求，只想使用微信小程序 推荐使用 [ **萤火小程序商城v1.0** ]，稳定性更好，对服务器要求低，技术栈少，二次开发更简便

注：二者并非升级关系，因产品理念和技术架构完全不同，属于两套产品，可根据需求进行选择

### ---------------------------------------------


### 萤火小程序商城v1.0

##### 项目介绍
萤火小程序商城v1.0，是一款开源的电商系统，为中小企业提供最佳的新零售解决方案。采用稳定的MVC框架开发，执行效率、扩展性、稳定性值得信赖。

##### 项目截图
![输入图片说明](https://gitee.com/uploads/images/2018/0629/144738_39b279a7_597459.png "前端.png")

##### 后台截图
![输入图片说明](https://gitee.com/uploads/images/2018/0629/144835_4e7858ef_597459.png "后台-商品列表.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0629/144851_2a4c1e50_597459.png "后台-新增商品.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0629/144952_acc1d20d_597459.png "后台-首页设计.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0629/145004_5a5ba42c_597459.png "后台-配送设置.png")

##### 环境要求
- Nginx/Apache/IIS
- PHP5.4+
- MySQL5.1+

建议使用环境：Linux + Nginx1.14 + PHP7.1 + MySQL5.6


##### 安全&缺陷
如果你发现了一个安全漏洞，请发送邮件到 developer@yiovo.com。所有的安全漏洞都将及时得到解决。

